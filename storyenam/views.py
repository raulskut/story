from django.shortcuts import render, redirect, get_object_or_404
from .models import Kegiatan, Peserta
from .forms import KegiatanForm, PesertaForm
from django.http import HttpResponseRedirect

# Create your views here.


def index(request):

    kegiatan = Kegiatan.objects.all()
    peserta = Peserta.objects.all()

    context = {
        'kegiatan': kegiatan,
        'peserta': peserta
    }

    return render(request, 'result-story-6.html', context)


def createActivity(request):

    if request.method == "POST":
        form = KegiatanForm(request.POST)

        if form.is_valid():
            form.save()
            return redirect('/storyenam')

    else:
        form = KegiatanForm()

    context = {
        'form': form
    }

    return render(request, 'formActivity-story-6.html', context)


def createName(request, id_nama):

    if request.method == 'POST':
        form = PesertaForm(request.POST)
        if form.is_valid():
            peserta = Peserta(ikutKegiatan=Kegiatan.objects.get(
                id=id_nama), nama_peserta=form.data['nama_peserta'])
            peserta.save()

        # return ke index
        return redirect('/storyenam')

    else:
        form = PesertaForm()

    return render(request, 'formName-story-6.html', {'form': form})


def delete(request, id_activity):
    Kegiatan.objects.filter(id=id_activity).delete()
    return redirect('/storyenam')


def deleteOrang(request, id_orang):
    Peserta.objects.filter(id=id_orang).delete()
    return redirect('/storyenam')
