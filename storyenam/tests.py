
# Create your tests here.

from django.test import TestCase, Client
# from django.test import LiveServerTestCase
from django.urls import resolve
from .views import index, createActivity, createName
from .models import Kegiatan, Peserta
# from selenium import webdriver
# from selenium.webdriver.chrome.options import Options
# import time

# Create your tests here.


class TestKegiatan(TestCase):
    def test_event_url_is_exist(self):
        response = Client().get('/storyenam/')
        self.assertEqual(response.status_code, 200)

    def test_event_index_func(self):
        found = resolve('/storyenam/')
        self.assertEqual(found.func, index)

    def test_event_using_template(self):
        response = Client().get('/storyenam/')
        self.assertTemplateUsed(response, 'result-story-6.html')


class TestTambahKegiatan(TestCase):
    def test_add_event_url_is_exist(self):
        response = Client().get('/storyenam/create-activity/')
        self.assertEqual(response.status_code, 200)

    def test_add_event_index_func(self):
        found = resolve('/storyenam/create-activity/')
        self.assertEqual(found.func, createActivity)

    def test_add_event_using_template(self):
        response = Client().get('/storyenam/create-activity/')
        self.assertTemplateUsed(response, 'formActivity-story-6.html')

    def test_event_model_create_new_object(self):
        kegiatan = Kegiatan(nama_kegiatan="PPW")
        kegiatan.save()
        self.assertEqual(Kegiatan.objects.all().count(), 1)

    def test_event_url_post_is_exist(self):
        response = Client().post(
            '/storyenam/', data={'nama_kegiatan': 'Coding'})
        self.assertEqual(response.status_code, 200)


class TestRegist(TestCase):
    def setUp(self):
        kegiatan = Kegiatan(nama_kegiatan="PPW")
        kegiatan.save()
        self.assertEqual(Kegiatan.objects.all().count(), 1)

    def test_regist_POST(self):
        response = Client().post('/storyenam/create-name/1/',
                                 data={'nama_peserta': 'bangjago'})
        self.assertEqual(response.status_code, 302)

    def test_regist_GET(self):
        response = self.client.get('/storyenam/create-name/1/')
        self.assertTemplateUsed(response, 'formName-story-6.html')
        self.assertEqual(response.status_code, 200)


class TestHapusNama(TestCase):
    def setUp(self):
        acara = Kegiatan(nama_kegiatan="Coding")
        acara.save()
        nama = Peserta(nama_peserta="Raul")
        nama.save()

    def test_hapus_url_post_is_exist(self):
        response = Client().post('/storyenam/deleteOrang/1/')
        self.assertEqual(response.status_code, 302)

    def test_hapus_url_is_exist(self):
        response = self.client.get('/storyenam/deleteOrang/1/')
        self.assertEqual(response.status_code, 302)


class TestHapusKegiatan(TestCase):
    def setUp(self):
        acara = Kegiatan(nama_kegiatan="Coding")
        acara.save()
        nama = Peserta(nama_peserta="Raul")
        nama.save()

    def test_hapus_url_activity_is_exist(self):
        response = Client().post('/storyenam/delete/1/')
        self.assertEqual(response.status_code, 302)

    def test_hapus_activity_url_is_exist(self):
        response = self.client.get('/storyenam/delete/1/')
        self.assertEqual(response.status_code, 302)


# class FunctionalTest(LiveServerTestCase):
#     def setUp(self):
#         chrome_setup = Options()
#         self.browser = webdriver.Chrome(
#             'C:/Users/Rayhan Maulana Akbar/Documents/semester III/PPW/chromedriver.exe',
#             chrome_options=chrome_setup
#         )

#         super(FunctionalTest, self).setUp()

#     def tearDown(self):
#         self.browser.implicitly_wait(5)
#         self.browser.quit()
#         super(FunctionalTest, self).tearDown()
