from django.db import models

# Create your models here.
class Schedule(models.Model):
    lesson = models.CharField(max_length=100)
    lecturer = models.CharField(max_length=100)
    credit = models.CharField(
        max_length=2,
        choices=[
            ('1', '1'),
            ('2', '2'),
            ('3', '3'),
            ('4', '4'),
            ('5', '5'),
            ('6', '6'),
        ],
        default=1,
    )
    year = models.CharField(
        max_length=50,
        choices=[
            ('1', 'Ganjil 2020/2021'),
            ('2', 'Genap 2020/2021'),
            ('3', 'Ganjil 2021/2022'),
            ('4', 'Genap 2021/2022'),
        ],
        default=1,
    )
    description = models.TextField()
    published = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{} {}".format(self.lecturer, self.lesson)

    